//Some Necessary (and possibly some unnecessary headers)
#import "PSSpecifier.h"
#import "AHKActionSheet/AHKActionSheet.h"
#import <Foundation/Foundation.h>
#import <Foundation/NSDistributedNotificationCenter.h>
#import <dlfcn.h>
#import <notify.h>
#import <objc/message.h>
#import <UIKit/UIKit.h>
#import <SpringBoard/SpringBoard.h>
#import <objc/runtime.h>
#import <ShotBlocker/ShotBlocker.h>
#import "UICustomSheetClasses/UICustomActionSheet.h"
// Useful Macros
#define ScreenSharedPng @"/var/mobile/Library/Documents/ScreenSharedDefault.png"
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )
#define ROOTVIEW [[[UIApplication sharedApplication] keyWindow] rootViewController]
#define ScreenSharedKey @"ScreenShared"
#define PreferencesPlist @"/var/mobile/Library/Preferences/com.alfadesigns.ScreenShared.plist"

#ifndef kCFCoreFoundationVersionNumber_iOS_5_0
#define kCFCoreFoundationVersionNumber_iOS_5_0 675.00
#endif
#ifndef kCFCoreFoundationVersionNumber_iOS_6_0
#define kCFCoreFoundationVersionNumber_iOS_6_0 793.00
#endif
// Social Share Frameworks
#import <Social/Social.h>
//#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AMInstagramActivity.h"

//#import <SpringBoard/SBApplication.h>