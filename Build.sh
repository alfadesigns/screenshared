#!/bin/bash


#mkdir ./DEBS/$(date +%Y%m%d_%H%M%S)

make clean package >> 'buildLog.txt'
mv buildLog.txt DEBS/$(date -d "today" +"%Y%m%d%H%M").txt

echo "Finished building. Package is in ./DEBS"