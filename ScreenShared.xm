%config(warnings=none);
#import "ScreenSharedHeaders.h"

@interface SBUIController
+ (id)sharedInstance;
- (id)contentView;
@end

//This method here retireves the setting switch value (ON/OFF)
static BOOL getScreenShared()
{
	NSDictionary *preferences = [NSDictionary dictionaryWithContentsOfFile:PreferencesPlist];
	return [[preferences objectForKey:ScreenSharedKey]boolValue];
}



//This method here sets the setting switch value (ON/OFF)
static void setScreenShared(BOOL value)
{
	NSMutableDictionary *preferences = [NSMutableDictionary dictionary];
	[preferences addEntriesFromDictionary:[NSDictionary dictionaryWithContentsOfFile:PreferencesPlist]];
	[preferences setObject:[NSNumber numberWithBool:value] forKey:ScreenSharedKey];
	[preferences writeToFile:PreferencesPlist atomically:YES];
}


@interface SBApplicationIcon
- (void)launchFromLocation:(int)arg;
- (id)displayName;

@end

#if __IPHONE_OS_VERSION_MAX_ALLOWED != __IPHONE_3_0
@interface UIPopoverController()
-(id)popoverView;
@end
#endif

@interface SBAwayController
-(BOOL)isLocked;
@end

//@interface ScreenSharedWindow : UIViewController <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
@interface ScreenSharedWindow : UIViewController

@property (nonatomic, retain) CAShapeLayer* cornerLayer;
@property (nonatomic, retain) UIView* cornerView;
@property(nonatomic,readonly,retain) UINavigationController *navigationController;  

+(id)sharedInstance;
-(void)startMonitoring;
-(void)stopMonitoring;
@end

@implementation ScreenSharedWindow

+(id)sharedInstance {
	// Setup instance for current class once
	static id sharedInstance = nil;
	static dispatch_once_t token = 0;
	dispatch_once(&token, ^{
		sharedInstance = [self new];
	});
	// Provide instance
	return sharedInstance;
}
//Stop Monitoring for Screenshots
//Potential use for blacklisting features
// Potential use for springboard only disabling of screenshots 
-(void)stopMonitoring {
	[[ShotBlocker sharedManager] stopDetectingScreenshots];
}

-(void)startMonitoring {
	[[ShotBlocker sharedManager] detectScreenshotWithImageBlock:^(UIImage *screenshot) {
		NSLog(@"Screenshot: %@", screenshot);	
		AHKActionSheet *actionSheet = [[AHKActionSheet alloc] initWithTitle:nil];		
		[actionSheet dismissAnimated:TRUE];						
		actionSheet.blurTintColor = [UIColor colorWithWhite:0.0f alpha:0.75f];
		actionSheet.blurRadius = 8.0f;
		actionSheet.buttonHeight = 50.0f;
		actionSheet.cancelButtonHeight = 50.0f;
		actionSheet.animationDuration = 0.5f;
		actionSheet.cancelButtonShadowColor = [UIColor colorWithWhite:0.0f alpha:0.1f];
		actionSheet.separatorColor = [UIColor colorWithWhite:1.0f alpha:0.3f];
		actionSheet.selectedBackgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
		UIFont *defaultFont = [UIFont fontWithName:@"Avenir" size:17.0f];
		actionSheet.buttonTextAttributes = @{ NSFontAttributeName : defaultFont,
			NSForegroundColorAttributeName : [UIColor whiteColor] };
		actionSheet.disabledButtonTextAttributes = @{ NSFontAttributeName : defaultFont,
			NSForegroundColorAttributeName : [UIColor grayColor] };
		actionSheet.destructiveButtonTextAttributes = @{ NSFontAttributeName : defaultFont,
			NSForegroundColorAttributeName : [UIColor redColor] };
		actionSheet.cancelButtonTextAttributes = @{ NSFontAttributeName : defaultFont,
			NSForegroundColorAttributeName : [UIColor whiteColor] };
		//HeaderView===============================
		UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
		UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/ScreenShared/ScreenSharedIcon.png"]];
		imageView.frame = CGRectMake(10, 10, 40, 40);
		[headerView addSubview:imageView];
		UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, 200, 20)];
		label1.text = @"ScreenShared";
		label1.textColor = [UIColor whiteColor];
		label1.font = [UIFont fontWithName:@"Avenir" size:17.0f];
		label1.backgroundColor = [UIColor clearColor];
		[headerView addSubview:label1];
		//EndHeaderView
		actionSheet.headerView = headerView;
		//======================================
		/*
[actionSheet addButtonWithTitle:@"Share via SMS"  
	image:[UIImage imageWithContentsOfFile:@"/Library/Application Support/ScreenShared/SMS.png"]
	type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
	NSLog(@"Share to FB tapped");
	//The Magic: 
	UIWindow *window = [[UIApplication sharedApplication] keyWindow];
	id viewController = window.rootViewController;
	if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {
//check if the device can send text messages
/*     if(![MFMessageComposeViewController canSendText]) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device cannot send text messages" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		return;
	}

	//set receipients

	//set message text
	//NSString * message = @"ScreenShared!";

	MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
	[messageController.messageComposeDelegate self];
// [messageController setBody:message];
	[messageController addAttachmentData: UIImageJPEGRepresentation(screenshot, 1.0) typeIdentifier:@"public.data" filename:@"image.jpeg"];
	// Present message view controller on screen
	[viewController presentViewController:messageController animated:YES completion:nil]; 
	
UIActivity *activity = [[AMInstagramActivity alloc] init];
NSArray *items = @[@"ScreenShared", screenshot];

UIActivityViewController *viewControllers = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:@[activity]];

[viewController presentViewController:viewControllers animated:YES completion:NULL];
}
}];
//End Magic 
*/



		[actionSheet addButtonWithTitle:@"Share via Facebook"  
image:[UIImage imageWithContentsOfFile:@"/Library/Application Support/ScreenShared/Facebook.png"]
type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
			NSLog(@"Share to FB tapped");
			//The Magic: 
			UIWindow *window = [[UIApplication sharedApplication] keyWindow];
			id viewController = window.rootViewController;
			if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {
				SLComposeViewController *facebookPostVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
				//	[facebookPostVC setInitialText:@"ScreenShared!"];
				[facebookPostVC addImage:screenshot];
				[viewController presentViewController:facebookPostVC animated:YES completion:nil];
			} 
		}];
		//End Magic

		[actionSheet addButtonWithTitle:@"Share via Twitter" 
image:[UIImage imageWithContentsOfFile:@"/Library/Application Support/ScreenShared/Twitter.png"]
type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
			NSLog(@"Share to Twitter tapped");
			//The Magic: 
			UIWindow *window = [[UIApplication sharedApplication] keyWindow];
			id viewController = window.rootViewController;
			if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {
				SLComposeViewController *twitterPostVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
				//	[twitterPostVC setInitialText:@"ScreenShared!"];
				[twitterPostVC addImage:screenshot];
				[viewController presentViewController:twitterPostVC animated:YES completion:nil];
			} 
		}];
		//End Magic
		
/* 		[actionSheet addButtonWithTitle:@"Gallery"  
image:[UIImage imageWithContentsOfFile:@"/Library/Application Support/ScreenShared/Instagram.png"]
type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
			NSLog(@"Share to MORE tapped");
			//The Magic: 
			UIWindow *window = [[UIApplication sharedApplication] keyWindow];
			id viewController = window.rootViewController;

			if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {	
EasyGalleryViewController  *imageViewController = [[EasyGalleryViewController  alloc] init];
[viewController presentViewController:imageViewController animated:YES completion:nil];
			}
		}]; // End Magic		
		 */
		[actionSheet addButtonWithTitle:@"More"  
image:[UIImage imageWithContentsOfFile:@"/Library/Application Support/ScreenShared/MORE.png"]
type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
			NSLog(@"Share to MORE tapped");
			//The Magic: 
			UIWindow *window = [[UIApplication sharedApplication] keyWindow];
			id viewController = window.rootViewController;
			if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_6_0) {	
				UIActivity *activity = [[AMInstagramActivity alloc] init];
				NSArray *items = @[@"ScreenShared", screenshot];

				UIActivityViewController *viewControllers = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:@[activity]];

				[viewController presentViewController:viewControllers animated:YES completion:NULL];
			}
		}]; // End Magic

		[actionSheet show];
		double dismissTime = 3.0; // This is where you set how long the actionSheet will be displayed before dismissing. Currently set to 3 seconds.
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(dismissTime * NSEC_PER_SEC));
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			[actionSheet dismissAnimated:TRUE];
		});
		

	}];
	
}

-(BOOL)_shouldCreateContextAsSecure {
	return YES;
}



@end

ScreenSharedWindow* window;
%hook UIApplication
- (id)init {
	self = %orig;
	if (self) {
		// init server when launched
		[[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
			NSLog(@"ScreenShared - Server init");
			if(getScreenShared()){ [[ScreenSharedWindow sharedInstance] startMonitoring]; } 
			else{ 
				NSLog(@"WebSocket - Server stop");
				[[ScreenSharedWindow sharedInstance] stopMonitoring]; 	
			}
		}];
	}
	return self;
}
%end

/* %hook SBUIController
-(void)applicationDidFinishLaunching:(id)arg1 {
	
int width=[[UIScreen mainScreen] bounds].size.width;
int height=[[UIScreen mainScreen] bounds].size.height;
CGPoint center=CGPointMake(width/2,height/2);

	%orig;
	
	[[ScreenSharedWindow sharedInstance] startMonitoring];

}
%end */

%hook SpringBoard

- (void)applicationDidFinishLaunching:(id)arg1 {
	
	// int width=[[UIScreen mainScreen] bounds].size.width;
	// int height=[[UIScreen mainScreen] bounds].size.height;
	// CGPoint center=CGPointMake(width/2,height/2);

	%orig;
	
	[[ScreenSharedWindow sharedInstance] startMonitoring];

}

%end


%hook PrefsListController
-(NSMutableArray *) specifiers {

	NSMutableArray *specifiers = %orig;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		PSSpecifier *specifier = [PSSpecifier preferenceSpecifierNamed:@"ScreenShared"
target:self
set:@selector(setScreenShared:specifier:)
get:@selector(getScreenShared:)
detail:Nil
cell:PSSwitchCell
edit:Nil];
		[specifier setIdentifier:ScreenSharedKey];
		[specifier setProperty:[NSNumber numberWithBool:YES] forKey:@"enabled"];
		[specifier setProperty:[NSNumber numberWithBool:YES] forKey:@"alternateColors"];
		[specifier setProperty:[UIImage imageWithContentsOfFile:@"/Library/Application Support/ScreenShared/screensharedd.png"] forKey:@"iconImage"];
		[specifier setProperty:@"Settings-ScreenShared" forKey:@"iconCache"];
		
		[specifiers insertObject:specifier atIndex:1];
	});
	
	return specifiers;
}

%new -(id)getScreenShared:(PSSpecifier*)specifier {
	return [NSNumber numberWithBool:getScreenShared()];
}

%new -(void)setScreenShared:(id)value specifier:(PSSpecifier *) specifier {
	setScreenShared([value boolValue]);
}

%end

%ctor
{
	%init();
	//[[ScreenSharedWindow sharedInstance] startMonitoring];
	//consider using observer here.


}