export GO_EASY_ON_ME = 1
ARCHS = armv7 arm64
#TARGET = iphone:clang
TARGET_IPHONEOS_DEPLOYMENT_VERSION = 6.0
THEOS_BUILD_DIR = Packages
SHARED_CFLAGS = -fobjc-arc
# export DEBUG=1
# DEBUG=1
THEOS_DEVICE_IP=10.0.0.7

include theos/makefiles/common.mk

TWEAK_NAME = ScreenShared
ScreenShared_CFLAGS = -fobjc-arc
ScreenShared_FILES = ScreenShared.xm AMInstagramActivity.m $(wildcard AHKActionSheet/*.m) $(wildcard ShotBlocker/*.m) $(wildcard UICustomSheetClasses/*.m)
#$(wildcard JDStatusBar/*.m)
ScreenShared_FRAMEWORKS = UIKit Foundation CoreGraphics QuartzCore Accelerate Security LocalAuthentication AssetsLibrary Social SystemConfiguration MessageUI AVFoundation
ScreenShared_PRIVATE_FRAMEWORKS = Preferences

include $(THEOS_MAKE_PATH)/tweak.mk


#SUBPROJECTS += ScreenSharedPrefs
include $(THEOS_MAKE_PATH)/aggregate.mk

after-install::
	install.exec "killall -9 Preferences, killall -9 SpringBoard"
